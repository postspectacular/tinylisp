package tinylisp;

import java.util.Stack;

public interface IFn {
    String exec(TinyLisp lisp, Stack<String> callStack);
}
