/*
 *   __  .__              .__  .__
 * _/  |_|__| ____ ___.__.|  | |__| ____________
 * \   __\  |/    <   |  ||  | |  |/  ___/\____ \
 *  |  | |  |   |  \___  ||  |_|  |\___ \ |  |_> >
 *  |__| |__|___|  / ____||____/__/____  >|   __/
 *               \/\/                  \/ |__|
 *
 * (c) Copyright 2013 PostSpectacular (http://postspectacular.com/)
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Karsten Schmidt <k at postspectacular dot com>
 */
package tinylisp;

import java.util.*;
import java.io.IOException;
import java.nio.file.Paths;
import java.nio.file.Files;

public class TinyLisp {

    private static class DefFn implements IFn {
        public String exec(TinyLisp lisp, Stack<String> callStack) {
            String key=null;
            HashMap<String, String> env = lisp.env.peek();
            while(!callStack.isEmpty()) {
                key = callStack.pop();
                env.put(key, lisp.varLookup(callStack.pop()));
            }
            return env.get(key);
        }
    }

    private static class IdentityFn implements IFn {
        public String exec(TinyLisp lisp, Stack<String> callStack) {
            return lisp.varLookup(callStack.pop());
        }
    }

    private static class PlusFn implements IFn {
        public String exec(TinyLisp lisp, Stack<String> callStack) {
            double v = Double.parseDouble(lisp.varLookup(callStack.pop()));
            while(!callStack.isEmpty()) {
                v += Double.parseDouble(lisp.varLookup(callStack.pop()));
            }
            return String.valueOf(v);
        }
    }

    private static class MinusFn implements IFn {
        public String exec(TinyLisp lisp, Stack<String> callStack) {
            double v = Double.parseDouble(lisp.varLookup(callStack.pop()));
            if (callStack.isEmpty()) {
                v = -v;
            } else {
                while(!callStack.isEmpty()) {
                    v -= Double.parseDouble(lisp.varLookup(callStack.pop()));
                }
            }
            return String.valueOf(v);
        }
    }

    private static class MultFn implements IFn {
        public String exec(TinyLisp lisp, Stack<String> callStack) {
            double v = Double.parseDouble(lisp.varLookup(callStack.pop()));
            while(!callStack.isEmpty()) {
                v *= Double.parseDouble(lisp.varLookup(callStack.pop()));
            }
            return String.valueOf(v);
        }
    }

    private static class DivFn implements IFn {
        public String exec(TinyLisp lisp, Stack<String> callStack) {
            double v = Double.parseDouble(lisp.varLookup(callStack.pop()));
            if (callStack.isEmpty()) {
                v = 1.0 / v;
            } else {
                while(!callStack.isEmpty()) {
                    v /= Double.parseDouble(lisp.varLookup(callStack.pop()));
                }
            }
            return String.valueOf(v);
        }
    }

    private static class PrnFn implements IFn {
        public String exec(TinyLisp lisp, Stack<String> callStack) {
            StringBuilder sb = new StringBuilder();
            while(!callStack.isEmpty()) {
                sb.append(lisp.varLookup(callStack.pop()));
                if (!callStack.isEmpty()) sb.append(" ");
            }
            System.out.println("# "+sb.toString());
            return null;
        }
    }

    Stack<HashMap<String, String>> env = new Stack<HashMap<String, String>>();
    HashMap<String, IFn> functions = new HashMap<String, IFn>();

    public TinyLisp() {
        this(new HashMap<String, String>());
    }

    public TinyLisp(HashMap<String, String> bindings) {
        env.push(bindings);
        functions.put("def", new DefFn());
        functions.put("identity", new IdentityFn());
        functions.put("prn", new PrnFn());
        functions.put("+", new PlusFn());
        functions.put("-", new MinusFn());
        functions.put("*", new MultFn());
        functions.put("/", new DivFn());
    }

    public String eval(String code) {
        long t0 = System.nanoTime();
        Stack<String> stack = new Stack<String>();
        StringTokenizer st = new StringTokenizer(code.replaceAll("\\s+|,"," "), "()[] ", true);
        while(st.hasMoreTokens()) {
            parseForm(st, stack);
        }
        System.out.println(String.format("# time: %fms", (System.nanoTime() - t0) * 1e-6));
        return varLookup(stack.pop());
    }

    public void log(Object x) {
        //System.out.println(x.toString());
    }

    protected void parseForm(StringTokenizer st, Stack<String> stack) {
        log("parse, curr stack: "+stack);
        while(st.hasMoreTokens()) {
            String tok = st.nextToken();
            log("p-tok: "+tok);
            if(tok.equals(")")) {
                interprete(stack);
                if (stack.size() < 2) return;
            } else if (tok.equals("let")) {
                stack.pop();
                log("pre-let-stack:  "+stack);
                stack.push(letBinding(st));
                log("post-let-stack: "+stack);
                if (stack.size() < 2) return;
            } else if (!tok.equals(" ")) {
                stack.push(tok);
            }
        }
    }

    private final String letBinding(StringTokenizer st) {
        Stack<String> scope = new Stack<String>();
        HashMap<String,String> scopeEnv = new HashMap<String, String>(env.peek());
        env.push(scopeEnv);
        log("let-intro, curr env: "+env);
        String tok=null;
        while(st.hasMoreTokens()) {
            tok = st.nextToken();
            log("let-token: "+tok);
            if (tok.equals("]")) {
                break;
            } else if (tok.equals("(")) {
                Stack<String> ls = new Stack<String>();
                ls.push(tok);
                parseForm(st, ls);
                newBinding(scope.pop(), ls.peek());
                log("letenv: "+scopeEnv);
            } else if (!tok.equals(" ") && !tok.equals("[")) {
                if (scope.isEmpty()) {
                    scope.push(tok);
                    log("scope: "+scope);
                } else {
                    newBinding(scope.pop(), varLookup(tok));
                    log("letenv: "+scopeEnv);
                }
            }
        }
        scope.push("(");
        scope.push("identity");
        while (st.hasMoreTokens() && scope.size() > 1) {
            parseForm(st, scope);
            log("let-stack: "+scope);
        }
        env.pop();
        return scope.pop();
    }

    private final void interprete(Stack<String> stack) {
        String tok;
        Stack<String> callStack = new Stack<String>();
        while(!(tok=stack.pop()).equals("(")){
            callStack.push(tok);
        }
        funcall(stack, callStack);
        log("post-fncall: "+stack+" cs: "+callStack);
    }

    private final void funcall(Stack<String> stack, Stack<String> callStack) {
        String func = callStack.pop();
        log("funcall: "+func+ " cs: "+callStack);
        IFn fn = functions.get(func);
        if (fn != null) {
            String val = fn.exec(this, callStack);
            if (val != null) {
                stack.push(val);
            }
        }
    }

    public void newBinding(String sym, String val) {
        log("new binding: "+sym+"="+val);
        env.peek().put(sym,val);
    }

    public String varLookup(String x) {
        String v = env.peek().get(x);
        return (v != null) ? v : x;
    }

    public static void main(String[] args) throws IOException {
        String code = null;
        String opt = args[0];
        if (opt.equals("-f")) {
            code = new String(Files.readAllBytes(Paths.get(args[1])),"UTF-8");
        } else {
            StringBuilder sb = new StringBuilder();
            for(int i=0; i<args.length; i++) {
                sb.append(args[i]).append(' ');
            }
            code=sb.toString();
        }
        System.out.println("# result: " + new TinyLisp().eval(code));
    }
}
