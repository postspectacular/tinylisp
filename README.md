# TinyLisp

      __  .__              .__  .__
    _/  |_|__| ____ ___.__.|  | |__| ____________
    \   __\  |/    <   |  ||  | |  |/  ___/\____ \
     |  | |  |   |  \___  ||  |_|  |\___ \ |  |_> >
     |__| |__|___|  / ____||____/__/____  >|   __/
                  \/\/                  \/ |__|

A toy Lisp interpreter using Clojure syntax (though not its
semantics), written in Java, with the aim of being, at some point,
potentially useful as mini embeddable DSL...

## Current features

* All fns implemented as classes implementing `tinylisp.IFn` interface
* Custom fns can be added to function table
* Interpreter constructor accepts pre-populated var bindings

### Implemented functions

* Math ops: `+`, `-`, `*`, `\`
* Identity: `identity`
* Printing to stdout `(prn ...)`

### Special forms

* symbol definition: `(def sym val...)`
* local scope definition & symbol bindings: `(let [sym val...] ...)`

## Compile

    git clone git@bitbucket.org:postspectacular/tinylisp.git
    cd tinylisp
    javac -d bin/ src/tinylisp/*.java

## Running / examples

When evaluating code via `./tinylisp`, code can either be supplied via
string argument or read from file using `-f path/to/source`.

    ./tinylisp "(+ 1 2)"
    # time: 3.264000ms
    # result: 3.0


    ./tinylisp "(def a 23) (def b (* 2 a)) (* a b)"
    # time: 3.197000ms
    # result: 1058.0


    cat examples/let01.tl
    (let [a 23
          b (+ a 42)
          c (let [a 10] (* a a))
          d (+ a b c)]
      (prn "a" a "b" b "c" c "d" d)
      (+ d (let [b d] (/ b 1000))))

    ./tinylisp -f examples/let01.tl
    # "a" 23 "b" 65.0 "c" 100.0 "d" 188.0
    # time: 5.249000ms
    # result: 188.188
